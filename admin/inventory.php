<?PHP
/****************************************
#	Inventory.php						#
#	Date Updated: 2/7/2012				#
****************************************/

session_start();

include("includes/functions.php");

include("../includes/global_functs.php");

include("../includes/config.php");

dbconn($db['host'], $db['username'], $db['password'], $db['database']);

if($config['inventory'] == "off"){
	die(); 
}


check();

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title><?PHP echo $config['sitename']; ?> Administration Control Panel</title>

<?PHP	include("./includes/tinymce.php"); ?>



</head>



<body>

<div align='center'><h2><?PHP echo $config['sitename']; ?> Administration Control Panel - Manage Inventory</h2><br />

  <form enctype="multipart/form-data" action="inventory.php" method="post">
    <input type="hidden" name="MAX_FILE_SIZE" value="1000000" />
    Choose a file to upload: <input name="uploaded_file" type="file" />
    <input type="submit" value="Upload" />
  </form> 
</div>

<h3>Existing Inventory Files:</h3>
<?php
if($_POST){
	//Сheck that we have a file
	if((!empty($_FILES["uploaded_file"])) && ($_FILES['uploaded_file']['error'] == 0)) {
	  //Check if the file is JPEG image and it's size is less than 350Kb
	  $filename = basename($_FILES['uploaded_file']['name']);
	  
		$filename2 = stripslashes($filename);
		$filename2 = str_replace(" ","_",$filename2);
		
	  $ext = substr($filename, strrpos($filename, '.') + 1);
	  /*if (($ext == "jpg") && ($_FILES["uploaded_file"]["type"] == "image/jpeg") && 
		($_FILES["uploaded_file"]["size"] < 350000)) {*/
		//Determine the path to which we want to save this file
		  $newname = '../inventory/'.$filename2;
			$newname2 = $config['url'].'/inventory/'.$filename2;
		  //Check if the file with the same name is already exists on the server
		  if (!file_exists($newname)) {
			//Attempt to move the uploaded file to it's new place
			if ((move_uploaded_file($_FILES['uploaded_file']['tmp_name'],$newname))) {
			   echo "It's done! The file has been saved as: ".$newname2;
			} else {
			   echo "Error: A problem occurred during file upload!";
			}
		  } else {
			 echo "Error: File ".$_FILES["uploaded_file"]["name"]." already exists";
		  }
	  /*} else {
		 echo "Error: Only .jpg images under 350Kb are accepted for upload";
	  }*/
	} else {
	 echo "Error: No file uploaded";
	}
}


 if ($handle = opendir('../inventory/')) {
   while (false !== ($file = readdir($handle)))
      {
          if ($file != "." && $file != "..")
	  {
          	$thelist .= '<li><a href="../inventory/'.$file.'">'.$file.'</a><br />';
			$thelist .= 'URL: <input name="textfield" type="text" id="textfield" value="'.$config['url'].'inventory/'.$file.'" size="50"  /></li>';
          }
       }
  closedir($handle);
  }

echo("<P><ul>");
echo $thelist;
echo("</ul></P>");

		echo("<hr>");

		echo("<a href=\"index.php\">Return to Menu</a> | <a href=\"logout.php\">Logout</a>");



?>

</body>

</html>

