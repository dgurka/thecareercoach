<?PHP
/****************************************
#	Inventory.php						#
#	Date Updated: 2/7/2012				#
****************************************/

session_start();

include("includes/functions.php");

include("../includes/global_functs.php");

include("../includes/config.php");

dbconn($db['host'], $db['username'], $db['password'], $db['database']);

if($config['inventory'] == "off"){
	die(); 
}


check();

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title><?PHP echo $config['sitename']; ?> Administration Control Panel</title>

<?PHP	include("./includes/tinymce.php"); ?>



</head>



<body>

<div align='center'><h2><?PHP echo $config['sitename']; ?> Administration Control Panel - Manage Events</h2><br />

  <form enctype="multipart/form-data" action="inventory.php" method="post">
    <input type="hidden" name="MAX_FILE_SIZE" value="1000000" />
    Choose a file to upload: <input name="uploaded_file" type="file" />
    <input type="submit" value="Upload" />
  </form> 
</div>

</body>

</html>
