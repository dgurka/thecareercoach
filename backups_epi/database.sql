-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 31, 2012 at 06:34 PM
-- Server version: 5.5.8
-- PHP Version: 5.3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `cms_bare`
--

-- --------------------------------------------------------

--
-- Table structure for table `epitest_boxes`
--

CREATE TABLE IF NOT EXISTS `epitest_boxes` (
  `boxnum` tinyint(2) NOT NULL DEFAULT '0',
  `pagenum` tinyint(2) NOT NULL DEFAULT '0',
  `imgtxt` char(3) NOT NULL DEFAULT '0',
  `content` text NOT NULL,
  `imgpath` varchar(255) NOT NULL DEFAULT '',
  `yturl` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `epitest_boxes`
--

INSERT INTO `epitest_boxes` (`boxnum`, `pagenum`, `imgtxt`, `content`, `imgpath`, `yturl`) VALUES
(1, 1, 'txt', '', '', ''),
(2, 1, 'txt', '', '', ''),
(3, 1, 'txt', '', '', ''),
(4, 1, 'txt', '', '', ''),
(5, 1, 'txt', '', '', ''),
(6, 1, 'txt', '', '', ''),
(1, 2, 'txt', '', '', ''),
(2, 2, 'txt', '', '', ''),
(3, 2, 'txt', '', '', ''),
(4, 2, 'txt', '', '', ''),
(5, 2, 'txt', '', '', ''),
(6, 2, 'txt', '', '', ''),
(1, 3, 'txt', '', '', ''),
(2, 3, 'txt', '', '', ''),
(3, 3, 'txt', '', '', ''),
(4, 3, 'txt', '', '', ''),
(5, 3, 'txt', '', '', ''),
(6, 3, 'txt', '', '', ''),
(2, 4, 'txt', '', '', ''),
(1, 4, 'txt', '', '', ''),
(3, 4, 'txt', '', '', ''),
(4, 4, 'txt', '', '', ''),
(5, 4, 'txt', '', '', ''),
(6, 4, 'txt', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `epitest_pages`
--

CREATE TABLE IF NOT EXISTS `epitest_pages` (
  `id` tinyint(2) NOT NULL DEFAULT '0',
  `title` varchar(25) NOT NULL DEFAULT '',
  `keywords` text NOT NULL,
  `unlocked` varchar(5) NOT NULL DEFAULT 'true',
  `visible` varchar(5) NOT NULL DEFAULT 'true'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `epitest_pages`
--

INSERT INTO `epitest_pages` (`id`, `title`, `keywords`, `unlocked`, `visible`) VALUES
(1, 'Home', 'keywords', 'true', 'true'),
(2, 'Page Two', 'keywords', 'true', 'true'),
(3, 'Page Three', 'keywords', 'true', 'true'),
(4, 'Page Four', 'keywords', 'true', 'true');

-- --------------------------------------------------------

--
-- Table structure for table `gallery1`
--

CREATE TABLE IF NOT EXISTS `gallery1` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `caption` varchar(255) DEFAULT NULL,
  `file_path` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `gallery1`
--


-- --------------------------------------------------------

--
-- Table structure for table `gallery2`
--

CREATE TABLE IF NOT EXISTS `gallery2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `caption` varchar(255) DEFAULT NULL,
  `file_path` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `gallery2`
--


-- --------------------------------------------------------

--
-- Table structure for table `msgboard`
--

CREATE TABLE IF NOT EXISTS `msgboard` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `name` varchar(65) NOT NULL DEFAULT '',
  `email` varchar(65) NOT NULL DEFAULT '',
  `comment` longtext NOT NULL,
  `datetime` varchar(65) NOT NULL DEFAULT '',
  `IP` varchar(25) NOT NULL,
  `approved` varchar(10) NOT NULL DEFAULT 'false',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `msgboard`
--

