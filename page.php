<?php

require_once("bootloader.php");
require_once("includes/db.php");
require_once("includes/global_functs.php");

$db = new Db();
$conn = $db->GetNewConnection();

if(!isset($pgid))
{
  $pgid = $_GET["id"];
}

$pgid = (int)$pgid;
$result = $db->ExecuteFirst("SELECT * FROM epitest_pages WHERE id = '$pgid'", $conn);

$result_boxes = $db->ExecuteQuery("SELECT * FROM epitest_boxes WHERE pagenum = '$pgid'", $conn);

$db->CloseConnection($conn);

$context = getContext(); // sets default contexts

$page = new stdClass();
$page->title = $result["title"];
$page->keywords = $result["keywords"];
$page->box = array();

foreach ($result_boxes as $box) {
  $content = "";
  if($box["imgtxt"] == "txt" || $box["imgtxt"] == "fb")
    $content = $box["content"];
  else if($box["imgtxt"] == "img")
    $content = "<img src='{$box["imgpath"]}' />";
  else if($box["imgtxt"] == "you")
  {
    ob_start();
    embed($box["yturl"]);
    $content = ob_get_clean();
  }
  $page->box[$box["boxnum"]] = $content;
}

$context["page"] = $page;

echo $twig->render('page_display.html', $context);