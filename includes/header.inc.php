<?php
include("includes/global_functs.php");
include("includes/config.php");
dbconn($db['host'], $db['username'], $db['password'], $db['database']);

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />

<link href="css/lightbox.css" rel="stylesheet" />
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript" src="js/lightbox.js"></script>

<style type="text/css">

body
{
    background-image: url("pattern.png");
    color: #ffddc9;
    font-family: Arial;
}

#outer_content
{
    width: 1024px;
    margin:0 auto;
    background-image: url("wrapper.png");
    height: 209px;
    background-repeat: no-repeat;;
    position: relative;;
}

#inner_content
{
    top:217px;
    margin:0 auto;
    width: 1024px;
    background-image: url("wapper_footer.png");
}

#inner_inner
{
    margin-left: 143px;
    width:781px;
}

#inner_inner a
{
    color: white;
}

#nav
{
    position: absolute;
    top:160px;
    left:285px;
}

#nav a
{
    text-decoration: none;
    color: #ffddc9;
    padding-right: 15px;
}

#nav a:hover 
{
    text-decoration: underline;
}

h1, h2, h3, h4, h5, h6
{
    color: white;
}

</style>

<style type="text/css">
.cont
{
    position: relative;
    width: 220px;
}

.reg
{
    border: solid #5e2323 3px;
}

.cont .mini
{
    position: absolute;
    right:-71px;
    bottom: -51px;
}

.col
{
    width: 300px;
}
</style>

<?php 
if(isset($pgid))
{
    getkeywords($pgid);
}  
else 
{ 
    getkeywords("1"); 
} 
include("includes/googanalytics.php"); 
?>

</head>
<body>

<div id="outer_content">
<div id="nav">
    <a href="index.php">Home</a>
    <a href="page.php?id=2">Custom Corsets</a>
    <a href="page.php?id=3">Measurements</a>
    <a href="gallery2.php?gal=1">Gallery</a>
    <a href="page.php?id=5">Pricing</a>
    <a href="page.php?id=6">Links</a>
    <a href="page.php?id=7">About Us</a>
    <a href="contact.php">Contact</a>
</div>
</div>
<div id="inner_content">
<div id="inner_inner">