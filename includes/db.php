<?php

/**
 * this is the mysqli implementation
 * 
 * @author Joseph Dotson
 * 
 * @todo
 * properly check the connection
 */
class Db
{
	public function GetNewConnection()
	{
		return new mysqli(DB_SERVER, DB_USER, DB_PASSWORD, DB_DATABASE);
	}
	public function CloseConnection($conn)
	{
		$conn->close();
	}

	public function CheckConnection($conn)
	{
		if($conn != null)
			return true;
		else
			return false;
	}

	public function EscapeString($string, $conn)
	{
		return $conn->escape_string($string);
	}

	public function ExecuteQuery($query, $conn)
	{
		$result = $conn->query($query);
		if($conn->errno)
			throw new Exception($conn->error, 1);

		$data = array();
		while($row = $result->fetch_assoc())
		{
			$data[] = $row;
		}

		$result->free();

		return $data;
	}
	public function ExecuteNonQuery($query, $conn)
	{
		$conn->query($query);
		if($conn->errno)
			throw new Exception($conn->error, 1);
	}
	public function ExecuteFirst($query, $conn)
	{
		$result = $conn->query($query);
		if($conn->errno)
			throw new Exception($conn->error, 1);

		$data = null;
		if($row = $result->fetch_assoc())
		{
			$data = $row;
		}

		$result->free();

		return $data;
	}

	public function GetLastInsertID($conn)
	{
		return $conn->insert_id;
	}
}