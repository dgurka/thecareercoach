<?php

require_once("bootloader.php");

$galleryid = (isset($_GET["gal"])) ? $_GET["gal"] : "1";
$page = (isset($_GET["page"])) ? $_GET["page"] : "1";
$galleryid = (int)$galleryid;
// constants
$_IMAGES_PER_PAGE = 16;

require_once("includes/db.php");
$db = new Db();
$conn = $db->GetNewConnection();

$total_images = $db->ExecuteFirst("SELECT COUNT(*) AS t FROM gallery{$galleryid}", $conn);
$total_images = (int)$total_images["t"];

$start = ($page - 1) * $_IMAGES_PER_PAGE;

$images = $db->ExecuteQuery("SELECT * FROM gallery{$galleryid} ORDER BY sortid,id ASC LIMIT $start, $_IMAGES_PER_PAGE", $conn);

foreach ($images as $key => $value) {
	$images[$key] = (object)$value;
}

$db->CloseConnection($conn);

$paging = new stdClass();
$paging->total_images = $total_images;
$paging->start = $start + 1;
$paging->rockbottom = $start + $_IMAGES_PER_PAGE;

if(($total_images < $start + $_IMAGES_PER_PAGE)){
	$paging->end = $total_images;
} else{ 
	$paging->end = $start + $_IMAGES_PER_PAGE;
}

$paging->gallery = $galleryid;

$paging->next = ($page + 1);
$paging->prev = ($page - 1);

$context = getContext(); // sets default contexts
$context["paging"] = $paging;
$context["images"] = $images;

echo $twig->render('gallery2.html', $context);