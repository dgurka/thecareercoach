<?php

require_once("bootloader.php");
require_once('includes/recaptchalib.php');

$publickey = "6Lc3qsgSAAAAAPSKnaR1KvtyGmQQcolTRQB4ryAt";
$privatekey = "6Lc3qsgSAAAAALU6LiLZrIMHOsogmtyEb-6TOhgh";

$context = getContext(); // sets default contexts
$context["recaptcha_html"] = recaptcha_get_html($publickey);

if($_SERVER["REQUEST_METHOD"] == "POST")
{
	$privatekey = "6Lc3qsgSAAAAALU6LiLZrIMHOsogmtyEb-6TOhgh";
	$resp = recaptcha_check_answer (
		$privatekey,
		$_SERVER["REMOTE_ADDR"],
		$_POST["recaptcha_challenge_field"],
		$_POST["recaptcha_response_field"]
	);
	if($resp->is_valid)
	{
		$to_email = $config['adminemail'];
		##$to_email = "don@enablepoint.com"; // are we testing?
		$name = $_POST['name'];
		$from_email = $_POST['email'];
		##$from_email2 = "noreply@".$config['domain'];
		$subject =  $config['sitename']." Contact Request";
		
		if(eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $from_email)) { 
			$mailheader = "From: ".$from_email."\r\n"; 	
			$from_email2 = $_POST['email'];
		} else { 
			$mailheader = "From: noreply@".$config['domain']."\r\n"; 
			$from_email2 = "noreply@".$config['domain'];
		}
		
		$mailheader .= "Content-type: text/html; charset=iso-8859-1\r\n"; 

		
		//$message = $_POST['message'];
		$message = htmlentities($_POST['message']);
		$message = nl2br($message);
		
		$body = "Name: $name\n E-Mail: $from_email\n Message:\n $message\n\n";			
		
		$sendmail = @mail($to_email, $subject, $body, "From: <$from_email2>");
		$sendmail2 = @mail("frank@enablepoint.com", $subject, $body, "From: <$from_email2>");

		if($sendmail){
			$context["mmessage"] = "<div style=\"color:#cd9f47; padding-left:15px;font-size:1.5em;\">Your message has been succesfully submitted!</div>";
		} else {
			$context["mmessage"] = "<div style=\"color:red; padding-left:15px;font-size:1.5em;\">An error has occurred and your message has not been sent. Please try again.</div>";
		}
	}
	else
	{
		$context["name"] = $_POST["name"];
		$context["email"] = $_POST["email"];
		$context["message"] = $_POST["message"];
		$context["mmessage"] = "<div style=\"color:red; padding-left:15px;font-size:1.5em;\">The reCAPTCHA wasn't entered correctly. Please try again.</div>";
	}
}

echo $twig->render('contact.html', $context);