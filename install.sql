-- Adminer 3.3.3 MySQL dump

SET NAMES utf8;
SET foreign_key_checks = 0;
SET time_zone = 'SYSTEM';
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `epitest_boxes`;
CREATE TABLE `epitest_boxes` (
  `boxnum` tinyint(2) NOT NULL default '0',
  `pagenum` tinyint(2) NOT NULL default '0',
  `imgtxt` char(3) NOT NULL default '0',
  `content` text NOT NULL,
  `imgpath` varchar(255) NOT NULL default '',
  `yturl` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `epitest_boxes` (`boxnum`, `pagenum`, `imgtxt`, `content`, `imgpath`, `yturl`) VALUES
(1,	1,	'txt',	'<p style=\"text-align: center;\"><strong><span style=\"font-size: xx-large;\">MANAGEMENT &amp; CONSULTING SERVICES</span></strong><br /><span style=\"font-size: large; color: #000080;\"><strong>SMALL BUSINESS - ENVIRONMENTAL - MUNICIPAL - INDUSTRIAL - CONSTRUCTION</strong></span></p>\r\n<p style=\"text-align: center;\">United Resource was founded in 2009 by the president of the company, David L. Guth, as a leader in large infrastructure developments and remediation. United Resource\'s core competency is our strategic and cost effective management of waste streams created by various industrial processes. United Resource is committed to excellence in providing our customers with expedient, clean, organized, and innovative services.</p>',	'',	''),
(2,	1,	'txt',	'',	'',	''),
(3,	1,	'txt',	'',	'',	''),
(4,	1,	'txt',	'',	'',	''),
(5,	1,	'txt',	'',	'',	''),
(6,	1,	'txt',	'',	'',	''),
(1,	2,	'txt',	'<h2>About Us</h2>\r\n<p>United Resource was founded in 2009 by the president of the company, David L. Guth, as a leader in large infrastructure developments and remediation. United Resource\'s core competency is our strategic and cost effective management of waste streams created by various industrial processes. United Resource is committed to excellence in providing our customers with expedient, clean, organized, and innovative services.</p>\r\n<p>With a wealth of experience United Resource has earned widely spread acceptance and <br />recognition in providing the following:</p>\r\n<table style=\"width: 100%;\" border=\"0\">\r\n<tbody>\r\n<tr>\r\n<td>\r\n<ul>\r\n<li>Pollution Prevention&nbsp;&nbsp;&nbsp;</li>\r\n<li>Storm Water Treatment &amp; Monitoring&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;</li>\r\n<li>Employee Training&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;</li>\r\n<li>Industrial Wastewater Characterizations</li>\r\n<li>Construction Administration&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;</li>\r\n<li>Compliance Testing&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;</li>\r\n<li>Sludge Disposal Evaluation &amp; Permitting</li>\r\n<li>Contract Management&nbsp;&nbsp; &nbsp;&nbsp;</li>\r\n<li>Environmental Management</li>\r\n<li>Infrastructure Management</li>\r\n</ul>\r\n</td>\r\n<td>\r\n<ul class=\"_mce_tagged_br\">\r\n<li>NPDES Permit Compliance</li>\r\n<li>Storm Water Pollution Prevention Plans</li>\r\n<li>Inspections</li>\r\n<li>Wastewater Minimization</li>\r\n<li>Construction Observation</li>\r\n<li>Operation &amp; Maintenance Assistance</li>\r\n<li>Quality Management</li>\r\n<li>Safety Management</li>\r\n<li>Utility Management</li>\r\n</ul>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p>United Resource is committed to protecting and improving America\'s water, infrastructure &amp; environment.<br /><br /><br /></p>',	'',	''),
(2,	2,	'txt',	'',	'',	''),
(3,	2,	'txt',	'',	'',	''),
(4,	2,	'txt',	'',	'',	''),
(5,	2,	'txt',	'',	'',	''),
(6,	2,	'txt',	'',	'',	''),
(1,	3,	'txt',	'<p><span style=\"color: #000080;\"><strong><span style=\"font-size: x-large;\">Services<br /></span></strong></span></p>\r\n<h3><span style=\"color: #000080;\"><strong><span style=\"font-size: x-large;\">SLURRY MANAGEMENT</span></strong></span></h3>\r\n<p>United Resource is a leader in providing environmental excellence on large infrastructure developments and remediation projects.&nbsp; We provide onsite management of operations to insure maximum productivity, minimize cost of disposal, reduce downtime, and increase profits through expedience at which we complete the tasks at hand.</p>\r\n<p>United Resource provides Slurry Mangement to include the following applications:</p>\r\n<table style=\"width: 100%;\" border=\"0\">\r\n<tbody>\r\n<tr>\r\n<td>\r\n<ul>\r\n<li>Diamond Grinding</li>\r\n<li>Saw Cutting</li>\r\n<li>Drilling</li>\r\n</ul>\r\n</td>\r\n<td>\r\n<ul>\r\n<li>Cold Milling</li>\r\n<li>Hydro Demolition</li>\r\n<li>Grooving</li>\r\n</ul>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p>&nbsp;<span style=\"color: #000080;\"><strong><span style=\"font-size: x-large;\"><img style=\"margin: 5px; vertical-align: middle;\" src=\"http://www.enablepointsupport.com/united/inventory/deq.png\" alt=\"\" width=\"75\" height=\"32\" />INSPECTIONS</span></strong></span></p>\r\n<p>United Resources offers DEQ-certified onsite inspections. Our certified inspectors will come to the requested site based on the needs of the client.&nbsp; United Resource will perform inspections regularly to ensure that necessary measures are properly installed, maintained, and appropriate for the site conditions.&nbsp; We will prepare and provide written reports, sampling, digitl photography, site map design, and Storm Water Pollution Prevention Plans.&nbsp; Our inspections insure our clients are in compliance with federal and state regulations.</p>\r\n<p>United Resource holds certifications to provide the following inspections:</p>\r\n<table style=\"width: 100%;\" border=\"0\">\r\n<tbody>\r\n<tr>\r\n<td>\r\n<ul>\r\n<li>Storm Water Pollution</li>\r\n<li>Underground Pipe</li>\r\n</ul>\r\n</td>\r\n<td>\r\n<ul>\r\n<li>SESC- Soil Erosion and Sediment Control</li>\r\n<li>NPDES Permitting</li>\r\n</ul>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<h3><strong><span style=\"color: #000080; font-size: x-large;\">TRAINING</span></strong></h3>\r\n<p>With over 20 years in various areas of expertise, United Resource offers on and offsite trainings to educate our client\'s employees on the best management practices, providing invaluable cost savings.<br /><br />United resource provides training in the following areas:</p>\r\n<table style=\"width: 100%;\" border=\"0\">\r\n<tbody>\r\n<tr>\r\n<td>\r\n<ul>\r\n<li>Storm Water Pollution Prevention</li>\r\n<li>Good Housekeeping Practices</li>\r\n<li>Operating Requirements&nbsp;&nbsp; &nbsp;&nbsp;</li>\r\n</ul>\r\n</td>\r\n<td>\r\n<ul>\r\n<li>Structural and Non-Structural Controls</li>\r\n<li>Spill Response/Clean-Up</li>\r\n<li>Material Storage/Handling</li>\r\n</ul>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>',	'',	''),
(2,	3,	'txt',	'',	'',	''),
(3,	3,	'txt',	'',	'',	''),
(4,	3,	'txt',	'',	'',	''),
(5,	3,	'txt',	'',	'',	''),
(6,	3,	'txt',	'',	'',	''),
(2,	4,	'txt',	'',	'',	''),
(1,	4,	'txt',	'',	'',	''),
(3,	4,	'txt',	'',	'',	''),
(4,	4,	'txt',	'',	'',	''),
(5,	4,	'txt',	'',	'',	''),
(6,	4,	'txt',	'',	'',	''),
(1, 5,  'txt',  '<p><span style=\"color: #000080;\"><strong><span style=\"font-size: x-large;\">Services<br /></span></strong></span></p>\r\n<h3><span style=\"color: #000080;\"><strong><span style=\"font-size: x-large;\">SLURRY MANAGEMENT</span></strong></span></h3>\r\n<p>United Resource is a leader in providing environmental excellence on large infrastructure developments and remediation projects.&nbsp; We provide onsite management of operations to insure maximum productivity, minimize cost of disposal, reduce downtime, and increase profits through expedience at which we complete the tasks at hand.</p>\r\n<p>United Resource provides Slurry Mangement to include the following applications:</p>\r\n<table style=\"width: 100%;\" border=\"0\">\r\n<tbody>\r\n<tr>\r\n<td>\r\n<ul>\r\n<li>Diamond Grinding</li>\r\n<li>Saw Cutting</li>\r\n<li>Drilling</li>\r\n</ul>\r\n</td>\r\n<td>\r\n<ul>\r\n<li>Cold Milling</li>\r\n<li>Hydro Demolition</li>\r\n<li>Grooving</li>\r\n</ul>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p>&nbsp;<span style=\"color: #000080;\"><strong><span style=\"font-size: x-large;\"><img style=\"margin: 5px; vertical-align: middle;\" src=\"http://www.enablepointsupport.com/united/inventory/deq.png\" alt=\"\" width=\"75\" height=\"32\" />INSPECTIONS</span></strong></span></p>\r\n<p>United Resources offers DEQ-certified onsite inspections. Our certified inspectors will come to the requested site based on the needs of the client.&nbsp; United Resource will perform inspections regularly to ensure that necessary measures are properly installed, maintained, and appropriate for the site conditions.&nbsp; We will prepare and provide written reports, sampling, digitl photography, site map design, and Storm Water Pollution Prevention Plans.&nbsp; Our inspections insure our clients are in compliance with federal and state regulations.</p>\r\n<p>United Resource holds certifications to provide the following inspections:</p>\r\n<table style=\"width: 100%;\" border=\"0\">\r\n<tbody>\r\n<tr>\r\n<td>\r\n<ul>\r\n<li>Storm Water Pollution</li>\r\n<li>Underground Pipe</li>\r\n</ul>\r\n</td>\r\n<td>\r\n<ul>\r\n<li>SESC- Soil Erosion and Sediment Control</li>\r\n<li>NPDES Permitting</li>\r\n</ul>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<h3><strong><span style=\"color: #000080; font-size: x-large;\">TRAINING</span></strong></h3>\r\n<p>With over 20 years in various areas of expertise, United Resource offers on and offsite trainings to educate our client\'s employees on the best management practices, providing invaluable cost savings.<br /><br />United resource provides training in the following areas:</p>\r\n<table style=\"width: 100%;\" border=\"0\">\r\n<tbody>\r\n<tr>\r\n<td>\r\n<ul>\r\n<li>Storm Water Pollution Prevention</li>\r\n<li>Good Housekeeping Practices</li>\r\n<li>Operating Requirements&nbsp;&nbsp; &nbsp;&nbsp;</li>\r\n</ul>\r\n</td>\r\n<td>\r\n<ul>\r\n<li>Structural and Non-Structural Controls</li>\r\n<li>Spill Response/Clean-Up</li>\r\n<li>Material Storage/Handling</li>\r\n</ul>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>',  '', ''),
(2, 5,  'txt',  '', '', ''),
(3, 5,  'txt',  '', '', ''),
(4, 5,  'txt',  '', '', ''),
(5, 5,  'txt',  '', '', ''),
(6, 5,  'txt',  '', '', ''),
(2, 6,  'txt',  '', '', ''),
(1, 6,  'txt',  '', '', ''),
(3, 6,  'txt',  '', '', ''),
(4, 6,  'txt',  '', '', ''),
(5, 6,  'txt',  '', '', ''),
(6, 6,  'txt',  '', '', '');

DROP TABLE IF EXISTS `epitest_pages`;
CREATE TABLE `epitest_pages` (
  `id` tinyint(2) NOT NULL default '0',
  `title` varchar(25) NOT NULL default '',
  `keywords` text NOT NULL,
  `unlocked` varchar(5) NOT NULL default 'true',
  `visible` varchar(5) NOT NULL default 'true'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `epitest_pages` (`id`, `title`, `keywords`, `unlocked`, `visible`) VALUES
(1,	'Home',	'keywords',	'true',	'true'),
(2,	'About Us',	'keywords',	'true',	'true'),
(3,	'Services',	'keywords',	'true',	'true'),
(4,	'Unused Page',	'keywords',	'true',	'true');

DROP TABLE IF EXISTS `gallery1`;
CREATE TABLE `gallery1` (
  `id` int(11) NOT NULL auto_increment,
  `caption` varchar(255) default NULL,
  `file_path` varchar(255) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `gallery2`;
CREATE TABLE `gallery2` (
  `id` int(11) NOT NULL auto_increment,
  `caption` varchar(255) default NULL,
  `file_path` varchar(255) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `msgboard`;
CREATE TABLE `msgboard` (
  `id` int(4) NOT NULL auto_increment,
  `name` varchar(65) NOT NULL default '',
  `email` varchar(65) NOT NULL default '',
  `comment` longtext NOT NULL,
  `datetime` varchar(65) NOT NULL default '',
  `IP` varchar(25) NOT NULL,
  `approved` varchar(10) NOT NULL default 'false',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



CREATE TABLE IF NOT EXISTS `directory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text NOT NULL,
  `content` text NOT NULL,
  `url` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=0 ;
